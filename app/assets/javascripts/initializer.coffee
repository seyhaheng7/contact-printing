ContactUpload.Initializer =
  exec: (pageName) ->
    if pageName && ContactUpload[pageName]
      ContactUpload[pageName]['init']()

  currentPage: ->
    return '' unless $('body').attr('id')

    bodyIds     = $('body').attr('id').split('_')
    pageName    = ''
    for bodyId in bodyIds
      pageName += ContactUpload.Util.capitalize(bodyId)
    pageName

  init: ->
    ContactUpload.Initializer.exec('Common')
    if @currentPage()
      ContactUpload.Initializer.exec(@currentPage())

$(document).on 'ready page:load', ->
  ContactUpload.Initializer.init()
