#= require rails-ujs
#= require jquery
#= require jquery_ujs
#= require bootstrap

#= require namespace
#= require common
#= require util
#= require initializer
